# coding=utf-8

import ClienteDAO

##############################
##############################
##############################
##############################
##############################
# a "Main" do CorrentistaMain está lá no final
# antes estão definidas as funções que representam as funcionalidades do CorrentistaMain


def login():
    """
    Realiza o login do cliente
    :return: o cliente logado com sucesso
    """
    print "Programa correntista iniciado."
    print "Digite o número da sua conta contendo o dígito verificador. (Exemplo: 12345-6)"

    while True:
        numero_lido = raw_input("Número da conta: ")
        numeros = numero_lido.split("-")

        if len(numeros) != 2:
            print "Digite o número da sua conta novamente no formato 12345-6."
        else:
            try:
                senha = raw_input("Digite sua senha de 4 dígitos: ")
                cliente = ClienteDAO.get_correntista(numero_lido, senha)

                if cliente is None:
                    print "Número da conta ou senha inválidos. Digite novamente."
                else:
                    print "Sessão iniciada. Olá {}.\n".format(cliente.apelido)
                    break
            except Exception as e:
                print(e)
                print "Digite o número da sua conta novamente no formato 12345-5."

    return cliente


def sacar(cliente_logado):
    """
    Operação de sacar para o cliente atualmente logado
    :param cliente_logado: cliente atualmente logado
    :return: retorna o cliente_logado com as informações atualizadas (se o saque foi efetuado)
    """
    saldo_atual = float(cliente_logado.saldo_atual)
    cliente_atualizado = cliente_logado

    print "Informe o valor que deseja sacar. Max 1000 reais."
    print "Saldo atual: {}.".format(saldo_atual)
    print "Caso deseje retornar ao menu anterior, digite -1"
    print "###############################"

    while True:
        try:
            print "############################### \n"

            saque = float(raw_input("Digite o valor que deseja sacar:"))

            if saque > 1000:
                print "O saldo máximo permitido é 1000 reais. Informe um valor menor para sacar."
            elif saque > saldo_atual:
                print "Seu saldo atual é {}. Informe um valor menor para sacar.".format(saldo_atual)
            elif saque <= 0:
                break
            else:

                cliente_retornado = ClienteDAO.alterar_saldo(cliente_logado, saldo_atual - saque)

                if cliente_retornado is not None:
                    cliente_atualizado = cliente_retornado
                    print "Operação de saque ocorreu com sucesso."
                    print "Novo saldo: {}".format(saldo_atual - saque)
                else:
                    print "Algo de errado ocorreu na operação."
                    print "Saldo atual: {}".format(saldo_atual)
                break
        except Exception as e:
            print "Digite o saque desejado novamente."

    return cliente_atualizado


def depositar(cliente_logado):
    """
    Operação de depositar para o cliente atualmente logado
    :param cliente_logado: cliente atualmente logado
    :return: retorna o cliente_logado com as informações atualizadas (se o depósito foi efetuado)
    """

    print "Informe o valor que deseja depositar. Max 3000 reais."
    print "Caso deseje retornar ao menu anterior, digite 0."
    print "###############################"

    saldo_atual = float(cliente_logado.saldo_atual)
    cliente_atualizado = cliente_logado

    while True:
        try:
            print "############################### \n"

            deposito = float(raw_input("Digite o valor que deseja depositar:"))

            if deposito > 3000:
                print "O depósito permitido é 3000 reais. Informe um valor menor para depositar."
            elif deposito <= 0:
                break
            else:
                cliente_retornado = ClienteDAO.alterar_saldo(cliente_logado, saldo_atual + deposito)
                if cliente_retornado is not None:
                    cliente_atualizado = cliente_retornado

                    print "Operação de depósito ocorreu com sucesso."
                    print "Novo saldo: {}".format(saldo_atual + deposito)
                else:
                    print "Algo de errado ocorreu na operação."
                    print "Saldo atual: {}".format(saldo_atual)
                break
        except Exception as e:
            print "Digite o depósito desejado novamente."

    return cliente_atualizado


def verificar_saldo(cliente_logado):
    """
    Operação de verificar o saldo (saldo + limite de cheque especial) do cliente logado
    :param cliente_logado: cliente atualmente logado
    :return: retorna o saldo total desse cliente
    """

    try:
        saldo_atual = float(cliente_logado.saldo_atual)
        cheque_especial = float(cliente_logado.cheque_especial)

        print "Seu saldo atual é {}".format(saldo_atual)
        print "Seu limite de cheque especial é {}".format(cheque_especial)
        print "Logo, seu limite total é {}".format(saldo_atual + cheque_especial)

    except Exception as e:
        print "[ERRO] Algo de errado ocorreu ao tentar calcular o limite especial."


def alterar_nome(cliente_logado):
    print "Informe o novo nome atualizado."
    print "Caso deseje retornar ao menu anterior, digite qualquer número."
    print "###############################"

    cliente_atualizado = cliente_logado

    while True:
        try:
            print "############################### \n"

            novo_nome = raw_input("Digite o novo nome:")

            if len(novo_nome) > 0 and novo_nome.replace(" ", "").isalpha():
                cliente_retornado = ClienteDAO.alterar_nome(cliente_logado, novo_nome)
                if cliente_retornado is not None:
                    cliente_atualizado = cliente_retornado

                    print "Atualização de nome ocorreu com sucesso."
                    print "Novo nome: {}".format(cliente_atualizado.nome_completo)
                else:
                    print "Algo de errado ocorreu na atualização de nome."
                    print "Nome atual: {}".format(cliente_atualizado.nome_completo)
                break
            else:
                print "Nome inválido. Digite novamente."
        except Exception as e:
            print "Digite o nome desejado novamente."

    return cliente_atualizado


#################################################
#################################################
#################################################
#################################################
#################################################

# Daqui pra baixo é a "Main"


cliente_logado = login()

while True:
    print "###############################"
    print "###############################"
    print "1 - Sacar dinheiro."
    print "2 - Depositar dinheiro."
    print "3 - Verificar saldo com limite especial."
    print "4 - Alterar nome completo."
    print "Qualquer outro valor - Finalizar sessão."
    print "###############################"

    operacao = raw_input("Digite o número da operação que deseja iniciar:")
    print "\n\n"
    print "###############################"
    print "###############################"

    if operacao == "1":
        cliente_logado = sacar(cliente_logado)
    elif operacao == "2":
        cliente_logado = depositar(cliente_logado)
    elif operacao == "3":
        verificar_saldo(cliente_logado)
    elif operacao == "4":
        cliente_logado = alterar_nome(cliente_logado)
    else:
        break

    print "###############################"
    print "###############################"
    print "Retornando ao menu anterior."
    print "Operação finalizada. \n"
    print "###############################"
    print "###############################"
    print "1 - Sim"
    print "Qualquer outro valor - Não"

    operacao = raw_input("Deseja realizar outra operação ?")

    if operacao != "1":
        break
