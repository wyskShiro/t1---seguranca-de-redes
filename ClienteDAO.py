# coding=utf-8

import sqlite3
from Cliente import Cliente


def get_correntista(numero_da_conta, senha_quatro_digitos):
    """
    :param numero_da_conta o número da conta do cliente logado no formato XXXXX-X
    :param senha_quatro_digitos a senha do cliente logado
    :return retorna o cliente encontrado no BD ou None se nenhum foi encontrado
    """

    conn = sqlite3.connect('banco_seguro.db')

    try:
        t = (numero_da_conta, senha_quatro_digitos)
        cursor = conn.execute("SELECT * FROM cliente "
                              "WHERE numero_conta=? AND senha_quatro_digitos=?", t)

        cliente = Cliente()

        for row in cursor:
            cliente.id_cliente = row[0]
            cliente.id_banco = row[1]
            cliente.numero_conta = row[2]
            cliente.senha_quatro_digitos = row[3]
            cliente.nome_completo = row[4]
            cliente.apelido = row[5]
            cliente.saldo_atual = row[6]
            cliente.cheque_especial = row[7]

            return cliente
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao retornar os dados do cliente."
    finally:
        conn.close()

    return None


def alterar_saldo(cliente_logado, quantia_nova):
    """
    :param cliente_logado contém todas as informações do cliente
    :param quantia_nova o saldo que será escrito no BD
    :return retorna o cliente com dados atualizados
    """

    t = (quantia_nova, cliente_logado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set saldo_atual = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o saldo do cliente."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_logado.numero_conta, cliente_logado.senha_quatro_digitos)

    return cliente_atualizado


def alterar_nome(cliente_logado, novo_nome):
    """
    :param cliente_logado contém todas as informações do cliente
    :param novo_nome o nome que será escrito no BD
    :return retorna o cliente com dados atualizados
    """
    t = (novo_nome, cliente_logado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set nome_completo = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o nome do cliente."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_logado.numero_conta, cliente_logado.senha_quatro_digitos)

    return cliente_atualizado








