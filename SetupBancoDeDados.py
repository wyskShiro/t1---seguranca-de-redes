# coding=utf-8

import sqlite3


def criar_banco():
    """
    Função de testes para setar o banco de dados e até cria-lo
    """
    conn = sqlite3.connect('banco_seguro.db')
    c = conn.cursor()

    conn.execute('''
    CREATE TABLE IF NOT EXISTS "banco" (
  "id_banco" integer PRIMARY KEY AUTOINCREMENT,
  "nome_completo" text NOT NULL,
  "lastro_total" double NOT NULL
);
    ''')

    conn.execute('''
CREATE TABLE IF NOT EXISTS "cliente" (
  "id_cliente" integer PRIMARY KEY AUTOINCREMENT,
  "id_banco" integer,
  "numero_conta" text NOT NULL,
  "senha_quatro_digitos" text NOT NULL,
  "nome_completo" text NOT NULL,
  "apelido" text NOT NULL,
  "saldo_atual" double NOT NULL,
  "cheque_especial" double NOT NULL,
  FOREIGN KEY (id_banco) REFERENCES "banco" (id_banco)
);
    ''')

    conn.execute('''

CREATE TABLE IF NOT EXISTS "gerente" (
  "id_gerente" integer PRIMARY KEY AUTOINCREMENT,
  "id_banco" integer,
  "login" integer NOT NULL,
  "senha" text NOT NULL,
  "nome_completo" text NOT NULL,
  FOREIGN KEY (id_banco) REFERENCES "banco" (id_banco)
);
    ''')
    conn.execute('''
CREATE TABLE IF NOT EXISTS "log" (
  "id_log" integer PRIMARY KEY AUTOINCREMENT,
  "id_cliente" integer,
  "id_gerente" integer,
  "operacao" text NOT NULL,
  "valor" double,
  "data_operacao" text NOT NULL,
  FOREIGN KEY (id_cliente) REFERENCES "cliente" (id_cliente),
  FOREIGN KEY (id_gerente) REFERENCES "gerente" (id_gerente)
);
    ''')

    banco = ("Banquinho", 1400)
    c.execute('INSERT INTO banco (nome_completo, lastro_total) VALUES (?, ?)', banco)

    clientes = [(1, '12345-5', '1234', 'Afonso Solano', 'F de faca', 1000.50, 100.00),
                (1, '23456-0', '1234', 'Diogo Braga', 'Didi', 2000.50, 200.00),
                (1, '54321-5', '1234', 'Beto Estrada', 'Beto', 3000.50, 300.00),
                ]
    c.executemany('INSERT INTO cliente (id_banco, numero_conta, senha_quatro_digitos, '
                  'nome_completo, apelido, saldo_atual, cheque_especial)'
                  'VALUES (?,?,?,?,?,?,?)', clientes)

    gerente = (1, 'login', '1234', 'Alexandre Ottoni')
    c.execute('INSERT INTO gerente (id_banco, login, senha, nome_completo)'
              ' VALUES (?,?,?,?)', gerente)

    conn.commit()
    conn.close()

"""
Descomentar e rodar o arquivo caso deseje criar o banco
"""
# criar_banco()
