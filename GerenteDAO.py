# coding=utf-8

import sqlite3
import datetime

from Gerente import Gerente
from Cliente import Cliente


class Operacao:
    """
    Um "Enum" para as operações a serem gravadas no Log
    """
    INSERIR = 1
    DELETAR = 2
    ATUALIZAR = 3


def get_gerente(login_lido, senha):
    """
    :param login_lido o login
    :param senha a senha do gerente
    :return retorna o cliente encontrado no BD ou None se nenhum foi encontrado
    """

    conn = sqlite3.connect('banco_seguro.db')

    try:
        t = (login_lido, senha)
        cursor = conn.execute("SELECT * FROM gerente "
                              "WHERE login=? AND senha=?", t)

        gerente = Gerente()

        for row in cursor:
            gerente.id_gerente = row[0]
            gerente.id_banco = row[1]
            gerente.login = row[2]
            gerente.senha = row[3]
            gerente.nome_completo = row[4]

            return gerente
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao retornar os dados do gerente."
    finally:
        conn.close()

    return None


def get_correntista(numero_da_conta):
    """
    :param numero_da_conta o número da conta do cliente logado no formato XXXXX-X
    :return retorna o cliente encontrado no BD ou None se nenhum foi encontrado
    """

    conn = sqlite3.connect('banco_seguro.db')
    t = (numero_da_conta, )

    try:
        cursor = conn.execute("SELECT * FROM cliente "
                              "WHERE numero_conta=?", t)

        cliente = Cliente()

        for row in cursor:
            cliente.id_cliente = row[0]
            cliente.id_banco = row[1]
            cliente.numero_conta = row[2]
            cliente.senha_quatro_digitos = row[3]
            cliente.nome_completo = row[4]
            cliente.apelido = row[5]
            cliente.saldo_atual = row[6]
            cliente.cheque_especial = row[7]

            return cliente
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao retornar os dados do cliente."
        print e
    finally:
        conn.close()

    return None


def alterar_apelido(gerente_logado, cliente_retornado, novo_apelido):
    """
    :param cliente_logado contém todas as informações do cliente
    :param novo_apelido o apelido que será escrito no BD
    :return retorna o cliente com dados atualizados
    """
    t = (novo_apelido, cliente_retornado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set apelido = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o apelido do cliente."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_retornado.numero_conta)
    inserir_log(cliente_atualizado.id_cliente, gerente_logado.id_gerente, Operacao.ATUALIZAR)

    return cliente_atualizado


def verificar_se_numero_conta_disponivel(novo_numero_conta):
    """
    Verifica se o número de conta informado está disponível para ser vinculado à um cliente

    :param novo_numero_conta o número da conta do cliente logado no formato XXXXX-X
    :return retorna um boolean se existe uma conta no BD com esse mesmo número
    """

    conn = sqlite3.connect('banco_seguro.db')
    t = (novo_numero_conta, )

    try:
        cursor = conn.execute("SELECT * FROM cliente "
                              "WHERE numero_conta=?", t)

        for _ in cursor:
            return False
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao retornar os dados do cliente."
    finally:
        conn.close()

    return True


def alterar_numero_conta(gerente_logado, cliente_retornado, novo_numero_conta):
    """
    :param gerente_logado contém todas as informações do gerente
    :param cliente_retornado contém todas as informações do cliente
    :param novo_numero_conta o número de conta do cliente que sobrescreverá no BD
    :return retorna o cliente com dados atualizados
    """
    t = (novo_numero_conta, cliente_retornado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set numero_conta = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o número de conta do cliente."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_retornado.numero_conta)
    inserir_log(cliente_atualizado.id_cliente, gerente_logado.id_gerente, Operacao.ATUALIZAR, novo_numero_conta)

    return cliente_atualizado


def alterar_senha_quatro_digitos(gerente_logado, cliente_retornado, nova_senha_quatro_digitos):
    """
    :param gerente_logado contém todas as informações do gerente
    :param cliente_logado contém todas as informações do cliente
    :param nova_senha_quatro_digitos a senha de quatro dígitos que será escrito no BD

    :return retorna o cliente com dados atualizados
    """

    t = (nova_senha_quatro_digitos, cliente_retornado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set senha_quatro_digitos = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar a senha de quatro dígitos do cliente."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_retornado.numero_conta)
    inserir_log(cliente_atualizado.id_cliente, gerente_logado.id_gerente, Operacao.ATUALIZAR)

    return cliente_atualizado


def get_limite_disponivel_banco(id_banco):
    """
    :param id_banco o id do banco a ser buscado
    :return retorna o lastro disponivel nesse banco para ser dado como limite de cheque especial
    """

    conn = sqlite3.connect('banco_seguro.db')
    t = (id_banco,)

    try:
        cursor = conn.execute("SELECT lastro_total FROM banco "
                              "WHERE id_banco=?", t)

        for row in cursor:
            return float(row[0])
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao retornar os dados do cliente."
        print e
    finally:
        conn.close()

    return -1


def aumentar_limite_cliente(gerente_logado, cliente_retornado, aumento_limite, limite_disponivel_banco):
    """
    :param gerente_logado contém todas as informações do gerente
    :param cliente_logado contém todas as informações do cliente
    :param aumento_limite o aumento de limite que será dado ao cliente
    :param limite_disponivel_banco o limite atual disponível no banco

    :return retorna o cliente com dados atualizados
    """
    novo_cheque_especial = float(cliente_retornado.cheque_especial) + aumento_limite
    t = (novo_cheque_especial, cliente_retornado.id_cliente)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE cliente set cheque_especial = ? where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o valor de cheque especial do cliente."
    finally:
        conn.close()

    novo_limite_banco = float(limite_disponivel_banco) - aumento_limite
    t = (novo_limite_banco, cliente_retornado.id_banco)
    conn = sqlite3.connect('banco_seguro.db')

    try:
        conn.execute("UPDATE banco set lastro_total = ? where id_banco = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao modificar o valor de lastro do banco."
    finally:
        conn.close()

    cliente_atualizado = get_correntista(cliente_retornado.numero_conta)
    inserir_log(cliente_atualizado.id_cliente, gerente_logado.id_gerente, Operacao.ATUALIZAR, novo_limite_banco)

    return cliente_atualizado


def cadastrar_cliente(gerente_logado, numero_conta, senha_quatro_digitos, nome_completo, apelido):
    """
    Cadastra o cliente com as informações fornecidas e 0 de limite e saldo

    :param gerente_logado: o gerente que está cadastrando o cliente
    :param numero_conta: numero da conta do novo cliente
    :param senha_quatro_digitos: a senha do novo cliente
    :param nome_completo: nome do novo cliente
    :param apelido: apelido do novo cliente

    :return: o novo cliente
    """

    conn = sqlite3.connect('banco_seguro.db')
    t = (gerente_logado.id_banco, numero_conta, senha_quatro_digitos, nome_completo, apelido, 0, 0)

    try:
        conn.execute('INSERT INTO cliente (id_banco, numero_conta, senha_quatro_digitos, '
                     'nome_completo, apelido, saldo_atual, cheque_especial)'
                     'VALUES (?,?,?,?,?,?,?)', t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao cadastrar os dados do cliente."
    finally:
        conn.close()

    cliente_recem_cadastrado = get_correntista(numero_conta)
    inserir_log(cliente_recem_cadastrado.id_cliente, gerente_logado.id_gerente, Operacao.INSERIR)

    return cliente_recem_cadastrado


def remover_cliente(gerente_logado, cliente_remover):
    """

    :param gerente_logado: o gerente logado
    :param cliente_remover: o Cliente que será removido do BD

    :return: True se a operação foi bem sucedida ou False se não foi
    """

    conn = sqlite3.connect('banco_seguro.db')
    t = (cliente_remover.id_cliente, )

    try:
        conn.execute("DELETE from cliente where id_cliente = ?", t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao cadastrar os dados do cliente."
        return False
    finally:
        conn.close()

    inserir_log(cliente_remover.id_cliente, gerente_logado.id_gerente, Operacao.DELETAR)

    return True


def inserir_log(id_cliente, id_gerente, operacao, valor = -1):
    """
    Insere na tabela de log informações sobre uma operação executada no banco de dados pelo gerente

    :param id_cliente: cliente que participou dessa ação
    :param id_gerente: gerente que participou dessa ação
    :param operacao: identificador numérico da operação
    :param valor: um valor (opcional) para ser atribuído ao log

    :return:
    """
    conn = sqlite3.connect('banco_seguro.db')
    data_operacao = datetime.datetime.now().strftime("%c")
    t = (id_cliente, id_gerente, operacao, valor, data_operacao)

    try:
        conn.execute('INSERT INTO log (id_cliente, id_gerente, operacao, valor, data_operacao)'
                     'VALUES (?,?,?,?,?)', t)
        conn.commit()
    except Exception as e:
        print "[ERRO] Algo de errado aconteceu ao cadastrar os dados no Log."
        return False
    finally:
        conn.close()

