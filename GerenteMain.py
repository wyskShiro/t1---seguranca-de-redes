# coding=utf-8

import GerenteDAO

##############################
##############################
##############################
##############################
##############################
# a "Main" do GerenteMain está lá no final
# antes estão definidas as funções que representam as funcionalidades do GerenteMain


def login():
    """
    Realiza o login do gerente
    :return: o gerente logado com sucesso
    """

    print "Programa gerente iniciado."
    print "Digite o seu login e senha."

    while True:
        login_lido = raw_input("Login: ")

        if len(login_lido) < 1:
            print "Digite o seu login novamente."
        else:
            try:
                senha = raw_input("Digite sua senha: ")
                gerente = GerenteDAO.get_gerente(login_lido, senha)

                if gerente is None:
                    print "Login ou senha inválidos. Digite novamente."
                else:
                    print "Sessão iniciada. Olá {}.\n".format(gerente.nome_completo)
                    break
            except Exception as e:
                print(e)
                print "Digite o seu login e senha novamente."

    return gerente


def alterar_apelido_cliente(gerente_logado):
    """
    Altera o apelido do cliente encontrado pelo seu número de conta

    :param gerente_logado: gerente atualmente logado
    :return:
    """
    cliente_retornado = encontrar_cliente()
    if cliente_retornado == "-1":
        return

    try:
        while True:
            print "\n"
            print "###############################"
            print "Apelido atual: {}".format(cliente_retornado.apelido)
            print "Informe o novo apelido atualizado."
            print "Caso deseje retornar ao menu anterior, digite -1."
            print "###############################"
            print "############################### \n"

            novo_apelido = raw_input("Digite o novo apelido:")
            novo_apelido = novo_apelido.lower()

            if len(novo_apelido) > 0 and novo_apelido.replace(" ", "").isalpha():
                cliente_atualizado = GerenteDAO.alterar_apelido(gerente_logado, cliente_retornado, novo_apelido)
                if cliente_atualizado is not None:
                    print "Atualização de apelido ocorreu com sucesso."
                    print "Novo apelido: {}".format(cliente_atualizado.apelido)
                else:
                    print "Algo de errado ocorreu na atualização de apelido."
                    print "Apelido atual: {}".format(cliente_retornado.apelido)
                break
            else:
                print "Apelido inválido. Digite novamente."
    except Exception as e:
        print "[ERRO] ao alterar o apelido do cliente."


def alterar_numero_conta_cliente(gerente_logado):
    """
    Altera o número da conta do cliente encontrado

    :param gerente_logado: o gerente logado
    :return:
    """

    cliente_retornado = encontrar_cliente()
    if cliente_retornado == "-1":
        return

    try:
        while True:
            print "\n"
            print "###############################"
            print "Número da conta atual: {}".format(cliente_retornado.numero_conta)
            print "Nome do cliente: {}".format(cliente_retornado.nome_completo)
            print "Informe o novo número da conta atualizado."
            print "Caso deseje retornar ao menu anterior, digite -1."
            print "###############################"
            print "############################### \n"

            novo_numero_conta = raw_input("Digite o novo número da conta: (os 5 dígitos, ex: 12345)")
            somatorio = 0
            try:
                for numero in novo_numero_conta:
                    somatorio += int(numero)
                digito_verificador = somatorio % 10
            except Exception as e:
                print "Número de conta inválido. Digite novamente."

            novo_numero_conta = novo_numero_conta + "-" + str(digito_verificador)

            if len(novo_numero_conta) > 0 and GerenteDAO.verificar_se_numero_conta_disponivel(novo_numero_conta):

                cliente_atualizado = GerenteDAO.alterar_numero_conta(gerente_logado, cliente_retornado,
                                                                     novo_numero_conta)
                if cliente_atualizado is not None:
                    print "Atualização de número da conta ocorreu com sucesso."
                    print "Novo número da conta: {}".format(cliente_atualizado.numero_conta)
                else:
                    print "Algo de errado ocorreu na atualização do número da conta."
                    print "Número da conta atual: {}".format(cliente_retornado.numero_conta)
                break
            else:
                print "Número de conta já utilizado ou inválido. Digite novamente."
    except Exception as e:
        print "Digite o número da conta desejado novamente."


def alterar_senha_quatro_digitos(gerente_logado):
    """
    Altera a senha de quatro dígios do cliente encontrado

    :param gerente_logado: o gerente logado
    :return:
    """

    cliente_retornado = encontrar_cliente()
    if cliente_retornado == "-1":
        return

    try:
        while True:
            print "\n"
            print "###############################"
            print "Senha de quatro dígitos atual: {}".format(cliente_retornado.senha_quatro_digitos)
            print "Nome do cliente: {}".format(cliente_retornado.nome_completo)
            print "Informe a nova senha de quatro dígitos atualizada."
            print "Caso deseje retornar ao menu anterior, digite -1."
            print "###############################"
            print "############################### \n"

            nova_senha_quatro_digitos = raw_input("Digite a nova senha de quatro dígitos:")

            if nova_senha_quatro_digitos == "-1":
                break

            if len(nova_senha_quatro_digitos) == 4:
                cliente_atualizado = GerenteDAO.alterar_senha_quatro_digitos(gerente_logado, cliente_retornado,
                                                                             nova_senha_quatro_digitos)
                if cliente_atualizado is not None:
                    print "Atualização da senha de quatro dígitos ocorreu com sucesso."
                    print "Nova senha de quatro dígitos da conta: {}".format(
                        cliente_atualizado.senha_quatro_digitos)
                else:
                    print "Algo de errado ocorreu na atualização da senha de quatro dígitos."
                    print "Senha de quatro dígitos da conta atual: {}".format(
                        cliente_retornado.senha_quatro_digitos)
                break
            else:
                print "Senha inválida. Digite novamente."
    except Exception as e:
        print "Digite a senha de quatro dígitos novamente."


def aumentar_limite_cheque_cliente(gerente_logado):
    cliente_retornado = encontrar_cliente()
    if cliente_retornado == "-1":
        return

    try:
        while True:
            limite_disponivel_banco = GerenteDAO.get_limite_disponivel_banco(cliente_retornado.id_banco)

            if limite_disponivel_banco == 0:
                print "\n"
                print "###############################"
                print "Não há limite para cheque especial disponível no seu banco."
                break
            elif limite_disponivel_banco == -1:
                print "[ERRO] Algo de errado ocorreu na operação de recuperar o limite disponível."
                break

            print "\n"
            print "###############################"
            print "Número da conta atual: {}".format(cliente_retornado.numero_conta)
            print "Nome do cliente: {}".format(cliente_retornado.nome_completo)
            print "Limite atual do cliente: {}".format(cliente_retornado.cheque_especial)
            print "Aumento disponível: {}".format(limite_disponivel_banco)
            print "Informe o aumento do limite de cheque especial."
            print "Caso deseje retornar ao menu anterior, digite -1."
            print "###############################"

            print "############################### \n"

            aumento_limite = float(raw_input("Digite o aumento do limite de cheque especial:"))

            if 0 < aumento_limite <= limite_disponivel_banco:
                cliente_atualizado = GerenteDAO.aumentar_limite_cliente(gerente_logado, cliente_retornado,
                                                                        aumento_limite, limite_disponivel_banco)
                if cliente_atualizado is not None:
                    print "Atualização do limite de cheque especial ocorreu com sucesso."
                    print "Novo limite de cheque especial da conta: {}".format(cliente_atualizado.cheque_especial)
                else:
                    print "Algo de errado ocorreu na atualização do limite de cheque especial."
                    print "Limite de cheque especial da conta atual: {}".format(cliente_retornado.cheque_especial)
                break
            else:
                print "Aumento de limite inválido. Digite outro valor."
    except Exception as e:
        print "Digite o valor do aumento de limite novamente."


def criar_novo_correntista(gerente_logado):
    while True:
        try:

            print "\n"
            print "###############################"
            print "###############################"
            print "Digite as informações solicitadas para cadastrar o novo cliente."
            print "Caso deseje cancelar o cadastro e retornar ao menu anterior, digite -1 em qualquer etapa."
            print "###############################"

            numero_conta = -1
            while True:
                numero_conta_input = raw_input("Digite um número de conta (5 dígitos somente): ")

                somatorio = 0
                try:
                    for numero in numero_conta_input:
                        somatorio += int(numero)
                    digito_verificador = somatorio % 10
                except Exception as e:
                    print "Número de conta inválido. Digite novamente."
                    continue

                numero_conta = numero_conta_input + "-" + str(digito_verificador)
                if not GerenteDAO.verificar_se_numero_conta_disponivel(numero_conta):
                    print "Número de conta inválido ou já utilizado. Digite novamente."
                else:
                    break

            senha_quatro_digitos = ""
            while True:
                senha_quatro_digitos = raw_input("Digite a nova senha de quatro dígitos:")

                if len(senha_quatro_digitos) != 4 or not senha_quatro_digitos.isdigit():
                    print "Senha inválida. Digite a senha novamente."
                else:
                    break

            nome_completo = ""
            while True:
                nome_completo = raw_input("Digite o nome completo:")

                if len(nome_completo) == 0 or not nome_completo.replace(" ", "").isalpha():
                    print "Nome inválido. Digite o nome novamente"
                else:
                    break

            apelido = ""
            while True:
                apelido = raw_input("Digite o apelido completo:")

                if len(apelido) == 0 or not apelido.replace(" ", "").isalpha():
                    print "Apelido inválido. Digite o apelido novamente"
                else:
                    break

            print "###############################"
            print "Número da conta: {}".format(numero_conta)
            print "Senha de quatro dígitos: {}".format(senha_quatro_digitos)
            print "Nome do cliente: {}".format(nome_completo)
            print "Apelido do cliente: {}".format(apelido)
            print "Caso deseje cadastrar um cliente com essas informações, digite 1"
            print "Caso deseje retornar ao menu anterior, digite -1."
            print "###############################"

            cadastrar_cliente = raw_input("Cadastrar o cliente? ")

            if cadastrar_cliente == "1":
                cliente_cadastrado = GerenteDAO.cadastrar_cliente(gerente_logado, numero_conta,
                                                                  senha_quatro_digitos, nome_completo, apelido)
                if cliente_cadastrado is not None:
                    print "Cliente {} cadastrado com sucesso.".format(cliente_cadastrado.apelido)
                else:
                    print "Algo não permitiu o cadastro do cliente. Tente novamente."
                break
            else:
                print "Cancelando o cadastro do cliente."
                break

        except Exception as e:
            print "[ERRO] Digite o valor do aumento de limite novamente."


def remover_correntista(gerente_logado):
    """
    Encontra um cliente e remove-o do banco de dados, caso a senha informada seja igual à sua senha cadastrada

    :param gerente_logado: gerente logado
    :return:
    """
    cliente_retornado = encontrar_cliente()

    if cliente_retornado == "-1":
        return

    try:
        while True:
            print "Cliente encontrado: {}.".format(cliente_retornado.nome_completo)
            print "Número da conta: {}.".format(cliente_retornado.numero_conta)
            print "Informe a senha para confirmar a deleção ou -1 para retornar ao menu anterior."
            senha_confimarcao = raw_input("Digite a senha da conta para confimar a exclusão:")

            if senha_confimarcao == "-1":
                break
            elif senha_confimarcao == cliente_retornado.senha_quatro_digitos:
                if GerenteDAO.remover_cliente(gerente_logado, cliente_retornado):
                    print "Cliente deletado com sucesso."
                else:
                    print "[ERRO] Deleção não pode ser efetuada."
                break
            else:
                print "Senha incorreta"
    except Exception as e:
        print "[ERRO] Informe a senha novamente."


def encontrar_cliente():
    """
    Encontra um cliente pelo seu número de conta

    :return: -1 se a operação for cancelada ou o objeto Cliente se foi encotrado
    """

    try:
        while True:
            print "Informe o número da conta do cliente"
            print "Caso deseje retornar ao menu anterior, digite -1."

            numero_conta = raw_input("Digite o número da conta:")
            if numero_conta == "-1":
                break
            elif len(numero_conta) < 6:
                print "Digite um número de conta válido"
                continue
            else:
                cliente_retornado = GerenteDAO.get_correntista(numero_conta)
                if cliente_retornado is not None:
                    return cliente_retornado
                else:
                    print "Cliente não encontrado. Digite novamente o número da conta."
    except Exception as e:
        print "[ERRO] ao encontrar o cliente"
    return -1


#################################################
#################################################
#################################################
#################################################
#################################################

# Daqui pra baixo é a "Main"

gerente_logado = login()

while True:
    print "###############################"
    print "###############################"
    print "1 - Alterar apelido de um cliente."
    print "2 - Alterar o número da conta de um cliente."
    print "3 - Alterar o a senha de quatro dígitos de um cliente."
    print "4 - Aumentar limite de cheque especial de um cliente."
    print "5 - Criar novo correntista"
    print "6 - Remover uma conta de um cliente"
    print "Qualquer outro valor - Finalizar sessão."
    print "###############################"

    operacao = raw_input("Digite o número da operação que deseja iniciar:")
    print "\n\n"
    print "###############################"
    print "###############################"

    if operacao == "1":
        alterar_apelido_cliente(gerente_logado)
    elif operacao == "2":
        alterar_numero_conta_cliente(gerente_logado)
    elif operacao == "3":
        alterar_senha_quatro_digitos(gerente_logado)
    elif operacao == "4":
        aumentar_limite_cheque_cliente(gerente_logado)
    elif operacao == "5":
        criar_novo_correntista(gerente_logado)
    elif operacao == "6":
        remover_correntista(gerente_logado)
    else:
        break

    print "\n\n"
    print "###############################"
    print "###############################"
    print "Retornando ao menu anterior."

    print "Operação finalizada. \n"
    print "###############################"
    print "1 - Sim"
    print "Qualquer outro valor - Não"

    operacao = input("Deseja realizar outra operação ?")

    if operacao != 1:
        break
