class Cliente:
    """
    Encapsula os dados do cliente
    """
    id_cliente = -1,
    id_banco = 1,
    numero_conta = 00000 - 0,
    senha_quatro_digitos = 0000,
    nome_completo = "",
    apelido = "",
    saldo_atual = 0,
    cheque_especial = 0

    def __init__(self, id_cliente = -1,
                 id_banco = 1,
                 numero_conta = "00000-0",
                 senha_quatro_digitos = 0000,
                 nome_completo = "",
                 apelido = "",
                 saldo_atual = 0,
                 cheque_especial = 0):
        """
        Inicializa um objeto cliente com as informacoes lidas do banco ou com valores padroes
        """
        self.id_cliente = id_cliente
        self.id_banco = id_banco
        self.numero_conta = numero_conta
        self.senha_quatro_digitos = senha_quatro_digitos
        self.nome_completo = nome_completo
        self.apelido = apelido
        self.saldo_atual = saldo_atual
        self.cheque_especial = cheque_especial