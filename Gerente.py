class Gerente:
    """
    Encapsula os dados do gerente
    """
    id_gerente = -1,
    id_banco = 1,
    login = 00000 - 0,
    senha = 0000,
    nome_completo = ""

    def __init__(self, id_gerente=-1,
                 id_banco=1,
                 login=0000,
                 senha="",
                 nome_completo=""):
        """
        Inicializa um objeto gerente com as informacoes lidas do banco ou com valores padroes
        """
        self.id_gerente = id_gerente
        self.id_banco = id_banco
        self.login = login
        self.senha = senha
        self.nome_completo = nome_completo
